import firebase from "firebase/app";
import "firebase/auth";

import axios from 'axios'

const url = '/api/user-signup-info'
const loginUrl = 'api/user-signin'

export default {
  state: {
    loggedInUser:
      localStorage.getItem("userInfo") != null
        ? JSON.parse(localStorage.getItem("userInfo"))
        : null,
    loading: false,
    error: null
  },
  getters: {
    loggedInUser: state => state.loggedInUser,
    loading: state => state.loading,
    error: state => state.error
  },
  mutations: {
    setUser(state, data) {
      state.loggedInUser = data;
      state.loading = false;
      state.error = null;
    },
    setLogout(state) {
      state.loggedInUser = null;
      state.loading = false;
      state.error = null;
      // this.$router.go("/");
    },
    setLoading(state, data) {
      state.loading = data;
      state.error = null;
    },
    setError(state, data) {
      state.error = data;
      state.loggedInUser = null;
      state.loading = false;
    },
    clearError(state) {
      state.error = null;
    }
  },
  actions: { 
    login({commit}, data) {
      commit("clearError");
      commit("setLoading", true);
      return axios.post(`${loginUrl}`, {
        //   User Details
        email: data.email,
        password: data.password
      })
      .then(function(res){
        commit("setLoading", false);
        if (res.data.status == 'error') {
          localStorage.removeItem("userInfo");
          commit("setError", res.data);
        } else {
          const loginUser = {
            loginBy: 'app login',
            loginKey: res.data.result
          }
          localStorage.setItem("userInfo", JSON.stringify(loginUser));
          commit("setUser", {uid: res}); 
        }
      }).catch((err) =>{
        localStorage.removeItem("userInfo");
        commit("setError", err);
      })
    },


    // signUserUp
    signUserUp({commit}, data) {
      commit("setLoading", true);
      commit("clearError");
      
      return axios.post(`${url}?user_id=${data.userId}`, {
        //   User Details
        firstName: data.userDetails.firstName,
        lastName: data.userDetails.lastName,
        email: data.userDetails.email,
        password: data.userDetails.password
      })
      .then(function(res){
        commit("setLoading", false);
        if (res.data.status == 'error') {
          localStorage.removeItem("userInfo");
          commit("setError", res.data)
        } else {
            localStorage.removeItem("userId");
            const loginUser = {
              loginBy: 'app login',
              loginKey: res.data.result._id
            }
            localStorage.setItem("userInfo", JSON.stringify(loginUser));
            commit("setUser", res.data); 
        }
      }).catch((err) =>{
          return(err)
      })
    },

    // signOut
    signOut({commit}) {
      var storeInfo = JSON.parse(localStorage.getItem('userInfo'));
      if (storeInfo) {
        if (storeInfo.loginBy == 'app login') {
          localStorage.removeItem("userInfo");
          commit("setLogout");
        }else{
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
            console.log('User signed out.')
            localStorage.removeItem("userInfo");
            location.reload();
          });
          commit("setLogout");
        }
      }else{
        var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
            console.log('User signed out.')
            // location.reload();
          });
          commit("setLogout");
      }
    }
  }
};
