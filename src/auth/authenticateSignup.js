export default (to, from, next) => {
    if (
      localStorage.getItem("userId") != null &&
      localStorage.getItem("userId").length > 0
    ) {
      next();
    } else {
      localStorage.removeItem("userId");
      next("/app/sessions/signIn");
    }
  };
  