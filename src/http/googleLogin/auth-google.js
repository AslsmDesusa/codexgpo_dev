import axios from 'axios'
const googleLoginUrl = '/api/user-signin/by/google'

class googleAuth {
    
    // verifying googel auth
    static checkUser(data){
        return axios.post(`${googleLoginUrl}`, {
          //   User Details
            email: data.email,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default googleAuth;