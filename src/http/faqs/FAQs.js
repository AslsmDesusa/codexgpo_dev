import axios from 'axios'
const faqsUrl = '/api/faqs'
const addFaqsUrl = '/api/add/faqs'

class googleAuth {

    // faqs list
    static listOfFaqs() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${faqsUrl}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // faqs list of selected val
    static listOfSelelectedVal(data){
        return axios.post(`${faqsUrl}s`, {
          //   faqs detials
            _ids: data._ids,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    // faqs delete
    static deleteFaqs(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${faqsUrl}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    // save FAQs
    static newFaqs(data){
        return axios.post(`${faqsUrl}`, {
          //   faqs detials
            Question: data.Question,
            Answer: data.Answer,
            Tags: data.Tags
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // add FAQs
    static addFaqs(data, _id){
        return axios.put(`${addFaqsUrl}?_id=${_id}`, {
          //   faqs detials
            faqs: data.faqs
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // delete FAQs
    static deleteFaqsEquipment(data, _id){
        return axios.post(`/api/delete/faqs?_id=${_id}`, {
          //   faqs detials
            faqs: data.faqs
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

       // updating FAQs
    static updateFaqsOnEquip(data, _id){
        return axios.put(`/api/update/faqs?_id=${_id}`, {
          //   faqs detials
          Question: data.Question,
          Answer: data.Answer
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default googleAuth;