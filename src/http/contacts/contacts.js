import axios from 'axios'

const url = '/api/contact-list'

const getContactByType = '/api/contact-list-manufacturer'

const contactById = '/api/contact-list-make'


class department {
    // get all equipment information
    static getContactData() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

     // get list of contact list by there type
     static contactByType(type) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${getContactByType}?type=${type}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get contact by id
    static contactById(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${contactById}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get all equipment information
    static deleteContacts(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    // saving new post
    static insertPost(data){
        return axios.post(`${url}`, {
            // personal Information
            type: data.type,
            name: data.name,
            email: data.email,
            phone: data.phone,
            designation: data.designation,
            
            // address Info
            address: data.address,
            company: data.company,
            city: data.city,
            country: data.country,
            state: data.state,
            pinCode: data.pinCode,
            remarks: data.remarks,
            tags: data.tags,  
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    //   updating old post
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            //   User Details
            type: data.type,
            name: data.name,
            email: data.email,
            phone: data.phone,
            designation: data.designation,
            
            // address Info
            address: data.address,
            company: data.company,
            city: data.city,
            country: data.country,
            state: data.state,
            pinCode: data.pinCode,
            remarks: data.remarks,
            tags: data.tags,  
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
}

export default department;