import axios from 'axios'

const url = '/api/upload-product/image'
const sitePrepReq = '/api/site-prep-requirement/image'


class uploadImageToS3 {
    static upload(formData, fileExt ,_id){
        console.log(formData)
        return axios.post( `${url}?_id=${_id}&fileExt=${fileExt}`, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    static uploadSitePrep(formData, fileExt, folderName){
        return axios.post( `${sitePrepReq}?folderName=${folderName}&fileExt=${fileExt}`, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }
}

export default uploadImageToS3;