import axios from 'axios'

// related equipment
const url = '/api/add/hospital-details'

const uploadFile ='/api/upload-file/hospital'

const prominenentHos = '/api/add/hospital-details/prominent'

class equipment {
    // get all equipment information
    static getHospitalDetails() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get all prominent hospital
    static getProminentHospital() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${prominenentHos}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // creating new Hospital
    static insertPost(data){
        return axios.post(`${url}`, {
            // Hospital Info
            name: data.name,
            address: data.address,
            city: data.city,
            pinCode: data.pinCode,
            state: data.state,
            speciality: data.speciality,
            category: data.category,
            totalNumberofBeds: data.totalNumberofBeds,
            noOfICUBeds: data.noOfICUBeds,
            noOfOperationTheatre: data.noOfOperationTheatre,
            hospitalStatus: data.hospitalStatus,
            departments: data.departments,
            typeOfHospital: data.typeOfHospital,
            attachment: data.attachment,
            status: data.status
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // updating hospital data
    static updatePost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            // Hospital Info
            name: data.name,
            address: data.address,
            city: data.city,
            pinCode: data.pinCode,
            state: data.state,
            speciality: data.speciality,
            category: data.category,
            totalNumberofBeds: data.totalNumberofBeds,
            noOfICUBeds: data.noOfICUBeds,
            noOfOperationTheatre: data.noOfOperationTheatre,
            hospitalStatus: data.hospitalStatus,
            departments: data.departments,
            typeOfHospital: data.typeOfHospital,
            attachment: data.attachment,
            status: data.status
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      static uploadFileExt(formData, _id, fileExt, folderName){
        return axios.post( `${uploadFile}?_id=${_id}&folderName=${folderName}&fileExt=${fileExt}`, 
            formData
        )
        .then(function(res){
            return(res)
        })
        .catch(function(err){
            throw(err)
        });
    }

    // get all equipment information
    static deleteHospital(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

}

export default equipment;