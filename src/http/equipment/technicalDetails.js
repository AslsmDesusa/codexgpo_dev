import axios from 'axios'

const url = '/api/technical-details-of-equipment'
const url2 = '/api/equipment'

class technicalDetails {

    // equipment Details
    static getTechnicalDetails(equipment) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url2}/${equipment}/technical-fields`)
               return resolve(res)
           }catch(err){
               return reject(err);
           } 
        })
    }

      // updateing previous data
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            technicalDetails: data.technicalDetails
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }


}

export default technicalDetails;