import axios from 'axios'

const url = '/api/equipment-basic-info'

// other models url
const otherModels = '/api/equipment-other-models'

// related equipment
const relatedUrl = '/api/related-equipment'

// duplicate equipment
const duplicateEquipment = '/api/duplicate/document'


class equipment {
    // get all equipment information
    static getBasicInfo() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // getting related other models
    static getOtherModels(equipment){
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${otherModels}?equipment=${equipment}`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
         })
    }

    // get all equipment information
    static deleteBasicInfo(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    static getEquipInfoById(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}/${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           } 
        })
    }

    // related equipment
    static relatedEquipment(data, _id){
        return axios.put(`${relatedUrl}?_id=${_id}`, {
          //   User Details
          relatedEquipment: data.relatedEquipment,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // related equipment
    static relatedEquipmentDelete(data, _id){
        return axios.post(`${relatedUrl}?_id=${_id}`, {
          //   User Details
          relatedEquipment: data.relatedEquipment,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // related equipment
    static duplicateEquipment(data, _id){
        return axios.post(`${duplicateEquipment}?_id=${_id}`, {
          //   User Details
          skuCode: data.skuCode,
          modelNo: data.modelNo
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    // creating user
    static insertPost(data){
        return axios.post(`${url}`, {
          //   User Details
            department: data.department,
            equipment: data.equipment,
            name: data.name,
            skuCode: data.skuCode,
            modelNo: data.modelNo,
            Type: data.Type,
            manufecture: data.manufecture,
            vendors: data.vendors,
            endOfLifeDate: data.endOfLifeDate,
            modelLaunchDate: data.modelLaunchDate,
            description: data.description,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // updating equipment data
    static updatePost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
          //   User Details
            department: data.department,
            equipment: data.equipment,
            name: data.name,
            skuCode: data.skuCode,
            modelNo: data.modelNo,
            Type: data.Type,
            manufecture: data.manufecture,
            vendors: data.vendors,
            endOfLifeDate: data.endOfLifeDate,
            modelLaunchDate: data.modelLaunchDate,
            description: data.description,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default equipment;