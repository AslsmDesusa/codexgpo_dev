import axios from 'axios'

const url = '/api/prominent-hospital'

const hospitals = '/api/add/hospital-details/prominent/by/multiple-ids'

class equipmentProminent {
    // getting data
    static getInsert(data){
        return axios.post(`${hospitals}`, {
            _ids: data._ids
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    // creating data
    static insertPost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            prominentHospitals: data.prominentHospitals
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // pulling data
    static pullPost(data, _id){
        return axios.post(`${url}?_id=${_id}`, {
            prominentHospitals: data.prominentHospitals
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default equipmentProminent;