import axios from 'axios'

const url = '/api/equipment'
const uploadFile = '/api/site-prep-requirement/equipment-details'
const removeUpload = '/api/site-prep-requirement/remove/equipment-details'

class equipmentCost {
    // equipment Details
    static getEquipmentDetails() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // delete equipment
    static deleteEquipmentDetails(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // equipment Details by department
    static getEquipmentDetailsByDepartment(department) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}/${department}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // creating data
    static insertPost(data){
        return axios.post(`${url}`, {
            equipment: data.equipment,
            facility: data.facility,
            department: data.department,
            technicalFields: data.technicalFields,
            ratingKeys: data.ratingKeys
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // updateing previous data
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            equipment: data.equipment,
            facility: data.facility,
            department: data.department,
            technicalFields: data.technicalFields,
            ratingKeys: data.ratingKeys
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

       // faqs list of selected val
    static uploadPost(data, _id){
        return axios.post(`${uploadFile}?_id=${_id}`, {
          //   site detials
          sitePrepRequirement: data.sitePrepRequirement,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      static removeUpload(data, _id){
        return axios.post(`${removeUpload}?_id=${_id}`, {
          //   remove sitePrepReq
          displayName: data.displayName,
          fileLink: data.fileLink
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default equipmentCost;