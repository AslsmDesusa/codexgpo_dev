import axios from 'axios'

// options key and value save url
const url = '/api/add-varients-keys-value'
// combine data save url
const url1 = '/api/add-varients-combinations'


class addVarients {

    // creating data
    static insertPost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            varientsOptions: data.varientsOptions
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // pulling data
    static insertPostUrl1(data, _id){
        return axios.put(`${url1}?_id=${_id}`, {
            varientsCombinations: data.varientsCombinations
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default addVarients;