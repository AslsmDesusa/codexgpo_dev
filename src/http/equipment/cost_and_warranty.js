import axios from 'axios'

const url = '/api/equipment-cost-and-warranty'


class equipmentCost {
    // get all equipment information
    // static getBasicInfo() {
    //     return new Promise(async (resolve, reject) => {
    //        try {
    //            const res = await axios.get(`${url}`)
    //            return resolve(res)
    //        }catch(err){
    //            return reject(err);
    //        }
    //     })
    // }

    // creating user
    static insertPost(data, _id){
        return axios.put(`${url}/${_id}`, {
          //   User Details
          // equipmentCost: data.equipmentCost,
          // installationCost: data.installationCost,
          // maintenanceCost: data.maintenanceCost,
          // operatingCost: data.operatingCost,
          // trainingCost: data.trainingCost,
          // serviceCost: data.serviceCost,
          // warranty: data.warranty

          PriceInstallationWarranty: data.PriceInstallationWarranty,
          AnnualMaintenanceContract: data.AnnualMaintenanceContract,
          AnnualAMCEscalation: data.AnnualAMCEscalation,
          ComprensiveMaintenanceContract: data.ComprensiveMaintenanceContract,
          AnnualCMCEscalation: data.AnnualCMCEscalation,
          OptionsSelected: data.OptionsSelected
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default equipmentCost;