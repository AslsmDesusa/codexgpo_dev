import axios from 'axios'

const Url = '/api/statutery-requirement'
const deleteUrl = '/api/statutery-requirement/remove'

class googleAuth {


    // faqs list of selected val
    static insertPost(data, _id){
        return axios.post(`${Url}?_id=${_id}`, {
          //   faqs detials
          statuteryRequirement: data.statuteryRequirement,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      static pullPost(data, _id){
        return axios.post(`${deleteUrl}?_id=${_id}`, {
          //   faqs detials
          displayName: data.displayName,
          fileLink: data.fileLink
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default googleAuth;