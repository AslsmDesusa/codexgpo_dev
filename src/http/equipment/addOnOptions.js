import axios from 'axios'

const url = '/api/add-on-options'

class addOnOptions {
    // creating data
    static insertPost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            addOnOptions: data.addOnOptions
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // pulling data
    static pullPost(data, _id){
        return axios.post(`${url}?_id=${_id}`, {
            addOnOptions: data.addOnOptions
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default addOnOptions;