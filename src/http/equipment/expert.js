import axios from 'axios'

const url = '/api/experts-and-kol'

const Contacts = '/api/contact-by-multiple_ids'

class equipmentCost {
    // getting data
    static getInsert(data){
        return axios.post(`${Contacts}`, {
            _ids: data._ids
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    // creating data
    static insertPost(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
            experts: data.experts
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // pulling data
    static pullPost(data, _id){
        return axios.post(`${url}?_id=${_id}`, {
            experts: data.experts
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default equipmentCost;