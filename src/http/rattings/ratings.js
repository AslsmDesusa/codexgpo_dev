import axios from 'axios'

const Url = '/api/rattings'

const userRattingsKeys = '/api/rattings/keys'

const getRatingKeysByEqupment = '/api/equipment/'


class googleAuth {


    // faqs list of selected val
    static insertPost(data, _id){
        return axios.put(`${Url}?_id=${_id}`, {
          //   faqs detials
          rattings: data.rattings,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

      // get all equipment information
      static getEquipmentDetailsByName(equipment) {
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(`${getRatingKeysByEqupment}${equipment}/technical-fields`)
                return resolve(res)
            }catch(err){
                return reject(err);
            }
          })
      }

      static pullPost(data, _id){
        return axios.post(`${Url}?_id=${_id}`, {
          //   faqs detials
          reviewedBy: data.reviewedBy,
          description: data.description
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

}

export default googleAuth;