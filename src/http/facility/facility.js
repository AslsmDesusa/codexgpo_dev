import axios from 'axios'

const url = '/api/add/facility'


class facility {
    // get all equipment information
    static getFaciltiyData() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get all equipment information
    static deleteFacility(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    // saving new post
    static insertPost(data){
        return axios.post(`${url}`, {
          //   User Details
          facility: data.facility,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    //   updating old post
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
          //   User Details
          facility: data.facility,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
}

export default facility;