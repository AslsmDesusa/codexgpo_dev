import axios from 'axios'

const url = '/api/check-accept-invitation'
const urlAccept = '/api/accept-invite-user'


class statusInvitation {
    static statusInvitation(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    static updateStatus(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.put(`${urlAccept}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // // creating user
    // static insertPost(data){
    //     return axios.post(`${url}`, {
    //       //   User Details
    //         email: data.email,
    //     })
    //     .then(function(res){
    //         console.log(res)
    //         return(res)
    //     }).catch((err) =>{
    //         console.log(err)
    //         return(err)
    //     })
    //   }

}

export default statusInvitation;