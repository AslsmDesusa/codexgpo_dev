import axios from 'axios'

const url = '/api/invite-user'


class mailer {
    static getUserByRols(rols) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}?rols=${rols}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    static deleteUserByRols(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // creating user
    static insertPost(data){
        return axios.post(`${url}`, {
          //   User Details
            email: data.email,
        })
        .then(function(res){
            console.log(res)
            return(res)
        }).catch((err) =>{
            console.log(err)
            return(err)
        })
      }

}

export default mailer;