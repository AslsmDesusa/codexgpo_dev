import axios from 'axios'

const url = '/api/department'


class department {
    // get all equipment information
    static getDepartmentData() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get all equipment information
    static deleteDepartment(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    // saving new post
    static insertPost(data){
        return axios.post(`${url}`, {
          //   User Details
          department: data.department,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    //   updating old post
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
          //   User Details
          department: data.department,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
}

export default department;