import axios from 'axios'

const url = '/api/consumable'

const url1 = '/api/consumable-spares'

const mulipleIds = '/api/consumable-by-multiple_ids'


class department {
    // addding consumable equipment in equipment data
    static addingSpares(data, _id){
        return axios.put(`${url1}?_id=${_id}`, {
            //   User Details
            consumables: data.consumables,
          })
          .then(function(res){
              return(res)
          }).catch((err) =>{
              return(err)
          })   
    }

    // pulling data
    static pullPost(data, _id){
        return axios.post(`${url1}?_id=${_id}`, {
            consumables: data.consumables
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    static gettingMultiple(data){
        return axios.post(`${mulipleIds}`, {
            //   User Details
            _ids: data._ids,
          })
          .then(function(res){
              return(res)
          }).catch((err) =>{
              return(err)
          })   
    }

    // end

    // get all equipment information
    static getConsumable() {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.get(`${url}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }

    // get all equipment information
    static deleteConsumable(_id) {
        return new Promise(async (resolve, reject) => {
           try {
               const res = await axios.delete(`${url}?_id=${_id}`)
               return resolve(res)
           }catch(err){
               return reject(err);
           }
        })
    }
    
    // saving new post
    static insertPost(data){
        return axios.post(`${url}`, {
          //   User Details
            name: data.name,
            cost: data.cost,
            description: data.description,
            tags: data.tags,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }

    //   updating old post
    static insertUpdate(data, _id){
        return axios.put(`${url}?_id=${_id}`, {
          //   User Details
            name: data.name,
            cost: data.cost,
            description: data.description,
            tags: data.tags,
        })
        .then(function(res){
            return(res)
        }).catch((err) =>{
            return(err)
        })
      }
}

export default department;