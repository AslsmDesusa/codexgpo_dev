import Vue from "vue";
import store from "./store";
import { isMobile } from "mobile-device-detect";
import Router from "vue-router";
import NProgress from "nprogress";
import authenticate from "./auth/authenticate";
import authenticateSignup from './auth/authenticateSignup'

Vue.use(Router);

// create new router

const routes = [
  {
    path: "/",
    component: () => import("./views/app"), //webpackChunkName app
    beforeEnter: authenticate,
    redirect: "/app/dashboards/dashboard.v1",

    children: [
      {
        path: "/app/dashboards",
        component: () => import("./views/app/dashboards"), //dashboard
        children: [
          {
            path: "dashboard.v1",
            component: () => import("./views/app/dashboards/dashboard.v1")
          },
          {
            path: "dashboard.v2",
            component: () => import("./views/app/dashboards/dashboard.v2")
          },
          {
            path: "dashboard.v3",
            component: () => import("./views/app/dashboards/dashboard.v3")
          },
          {
            path: "dashboard.v4",
            component: () => import("./views/app/dashboards/dashboard.v4")
          },
          {
            path: "dashboard.v5",
            component: () => import("./views/app/dashboards/equipmentDashboard")
          },
          {
            path: "project-dashboard",
            component: () => import("./views/app/dashboards/projectDashboard")
          }
        ]
      },

      
      //  apps
      {
        path: "/app/apps",
        component: () => import("./views/app/apps"),
        redirect: "/app/apps/chat",
        children: [
          {
            path: "departmentMaster",
            component: () => import("./views/app/apps/departmentMaster")
          },
          {
            path: "consumable",
            component: () => import("./views/app/apps/consumable.vue")
          },
          {
            path: "hospitalMaster",
            component: () => import("./views/app/apps/hospitalMaster")
          },
          {
            path: 'equipmentMaster',
            component: () => import("./views/app/apps/equipmentMaster")
          },
          {
            path: 'projects',
            component: () => import("./views/app/apps/projects")
          },
          {
            path: "calendar",
            component: () => import("./views/app/apps/calendar")
          },
          {
            path: "contact-search",
            component: () => import("./views/app/apps/contact-search")
          },
          {
            path: "contact-list",
            component: () => import("./views/app/apps/contact-list")
          },
          {
            path: "faq-list",
            component: () => import("./views/app/apps/FAQs")
          }
        ]
      },

      //  Settings
      {
        path: "/settings",
        component: () => import("./views/settings"),
        children: [
          {
            path: "user-access",
            component: () => import("./views/settings/settings_access")
          },
        ]
      },

      // requirements 
      {
        path: "/requirements/requirement/hospital_requirement",
        component: () => import("./components/forms/requirement/hospital_requirement.vue")
      },
    ]
  },
  // sessions
  {
    path: "/app/sessions",
    component: () => import("./views/app/sessions"),
    redirect: "/app/sessions/signIn",
    children: [
      {
        path: "signIn",
        component: () => import("./views/app/sessions/signIn")
      },
      {
        path: "forgot",
        component: () => import("./views/app/sessions/forgot")
      },
      {
        path: "seeInvitation",
        component: () => import("./views/app/sessions/seeInvitation")
      }
    ]
  },

  // restricted Area
  {
    path: "/app/sessions",
    component: () => import("./views/app/sessions"),
    beforeEnter: authenticateSignup,
    redirect: "/app/sessions/signIn",
    children: [
      {
        path: "signUp",
        component: () => import("./views/app/sessions/signUp")
      },
    ]
  },
  
  {
    path: "/switch-menu",
    component: () => import("./containers/layouts/switchSidebar")
  },
  {
    path: "/icons",
    component: () => import("./views/app/pages/icons")
  },

  {
    path: "*",
    component: () => import("./views/app/pages/notFound")
  }
];

const router = new Router({
  // mode: "history",
  linkActiveClass: "open",
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  // If this isn't an initial page load.
  if (to.path) {
    // Start the route progress bar.

    NProgress.start();
    NProgress.set(0.1);
  }
  next();
});

router.afterEach(() => {
  // Remove initial loading
  const gullPreLoading = document.getElementById("loading_wrap");
  if (gullPreLoading) {
    gullPreLoading.style.display = "none";
  }
  // Complete the animation of the route progress bar.
  setTimeout(() => NProgress.done(), 500);
  // NProgress.done();
  // if (isMobile) {
  if (window.innerWidth <= 1200) {
    // console.log("mobile");
    store.dispatch("changeSidebarProperties");
    if (store.getters.getSideBarToggleProperties.isSecondarySideNavOpen) {
      store.dispatch("changeSecondarySidebarProperties");
    }

    if (store.getters.getCompactSideBarToggleProperties.isSideNavOpen) {
      store.dispatch("changeCompactSidebarProperties");
    }
  } else {
    if (store.getters.getSideBarToggleProperties.isSecondarySideNavOpen) {
      store.dispatch("changeSecondarySidebarProperties");
    }

    // store.state.sidebarToggleProperties.isSecondarySideNavOpen = false;
  }
});

export default router;